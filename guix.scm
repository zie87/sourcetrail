(use-modules (guix)
             (guix build-system cmake)
             ((guix licenses)
              #:prefix license:)
             (guix git-download)
             ((guix licenses)
              #:prefix license:)
             (gnu packages boost)
             (gnu packages llvm)
             (gnu packages qt)
             (gnu packages version-control))

(define vcs-file?
  ;; Return true if the given file is under version control.
  (or (git-predicate (current-source-directory))
      (const #t)))
 ;not in a Git checkout

(package
  (name "sourcetrail")
  (version "2024.1.0")
  (source
   (local-file "."
               "sourcetrail-checkout"
               #:recursive? #t
               #:select? vcs-file?))
  (build-system cmake-build-system)
  (arguments
   (list
    #:configure-flags #~(list "-DBoost_USE_STATIC_LIBS=OFF"
                              "-DBUILD_CXX_LANGUAGE_PACKAGE=ON")
    #:phases #~(modify-phases %standard-phases
                 (replace 'install
                   (lambda* (#:key outputs #:allow-other-keys)
                     (let* ((out #$output)
                            (prefix (string-append out "/opt/sourcetrail"))
                            (build (getcwd))
                            (share (string-append prefix "/share")))
                       (mkdir-p (string-append out "/bin"))
                       (install-file (string-append build "/app/Sourcetrail")
                                     (string-append out "/bin"))
                       (install-file (string-append build
                                      "/app/sourcetrail_indexer")
                                     (string-append out "/bin"))))))))
  (inputs (list boost clang-toolchain-16 llvm-16 qtbase-5 qtsvg-5))
  (synopsis "Interactive source explorer")
  (description
   "Offline source explorer that helps you get productive on unfamiliar source code with optional IDE integration.")
  (home-page "https://www.sourcetrail.com")
  (license license:gpl3))
