# vim: set ft=make :
root_dir := justfile_directory()


default:
	@just --list

_run-in-guix-shell cmd *args:
	#!/usr/bin/env -S sh -eu
	guix shell just bash ninja --development -f guix.scm -- just {{ cmd }} {{ args }}

_cmake-config:
	cmake -S. -B build/gcc -G "Ninja Multi-Config" -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DBoost_USE_STATIC_LIBS=OFF -DBUILD_CXX_LANGUAGE_PACKAGE=ON

_cmake-build target: (_cmake-config)
	cmake --build build/gcc --config {{target}}

_cmake-install target: (_cmake-build target)
	cmake --install build/gcc --prefix build/install/gcc --config {{target}}

# configure build
config: (_run-in-guix-shell "_cmake-config")

# build target (`just build Debug`)
build target: (_run-in-guix-shell "_cmake-build" target)

# install target (`just install Debug`)
install target: (_run-in-guix-shell "_cmake-install" target)

# delete build dir
clean: 
	rm -rf build


